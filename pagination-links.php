									<div class="pagination">
										<p>Page:</p>
										<ul>
<?php	for($i = 0; $i < $page_count; $i++): ?>
<?php		if($i == $page) :	// on this page ?>
											<li><?=$i+1?> </li>
<?php		else: ?>
<?php			if(empty($base_query)): ?>
											<li><a href="<?=$base_url ?>?page=<?=$i?>"><?=$i+1?></a> </li>
<?php			else: ?>
											<li><a href="<?=$base_url ?>?<?=$base_query ?>&page=<?=$i?>"><?=$i+1?></a> </li>
<?php			endif; ?>
<?php		endif; ?>
<?php	endfor; ?>
										</ul>
									</div>
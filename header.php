<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
<?php if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/themes/palette.css')): ?>
		<link rel="stylesheet" href="/themes/palette.css" type="text/css" media="screen" charset="utf-8" />
<?php else: ?>
        <link rel="stylesheet" href="/<?=Theme::theme()->theme_path ?>/palette.css" type="text/css" media="screen" charset="utf-8" />
<?php endif; ?>
		<link rel="stylesheet" href="/<?=Theme::theme()->theme_path ?>/common.css" media="screen" type="text/css" charset="utf-8" />
<?php if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/themes/site.css')): ?>
		<link rel="stylesheet" href="/themes/site.css" type="text/css" media="screen" charset="utf-8" />
<?php endif; ?>
		<link rel="stylesheet" href="/components/font-awesome/css/font-awesome.min.css" type="text/css" media="all" charset="utf-8" />
<?php
	$stylesheets = Theme::theme()->stylesheets;
	foreach($stylesheets as $stylesheet) {
		echo "\t\t<link rel=\"stylesheet\" href=\"" . $stylesheet->url . "\" type=\"text/css\" media=\"" . $stylesheet->media . "\" charset=\"utf-8\" />\n";
	}
?>
		<title><?=Theme::theme()->site_title ?></title>
	</head>
<?php if(count(Theme::theme()->global_classes) > 0): ?>
	<body class="<?=join(' ', Theme::theme()->global_classes) ?>">
<?php else: ?>
	<body>
<?php endif; ?>
		<header>
			<label for="nav-toggle" id="nav-toggle-button"><i class="fa fa-bars" aria-hidden="true"></i></label>
			<div>
				<h1><a href="/"><?=Theme::theme()->site_title ?></a></h1>
			</div>
		</header>
		<div>
			<main>

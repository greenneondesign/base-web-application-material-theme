			</main>
			<input type="checkbox" name="nav-toggle" id="nav-toggle" class="checkbox-hack">
			<aside>
				<label for="nav-toggle" class="scrim"></label>
				<nav>
<?php	if(\Users\User::is_user_authenticated()): ?>
					<header>
						<i class="fa fa-5x fa-user"></i>
						<?=\Users\User::get_authenticated_user()->get_display_name()?>
						<div class="button-group">
							<a href="/users/profile">Profile</a><a href="/users/logout">Logout</a>
						</div>
					</header>
<?php	else: ?>
					<header>
						<div class="button-group">
							<a href="/users/login" class="default">Login</a>
						</div>
					</header>
<?php	endif; ?>
<?php	if(isset(Theme::theme()->include_in_navigation) && is_array(Theme::theme()->include_in_navigation)): ?>
					<ul>
<?php		foreach(Theme::theme()->include_in_navigation as $static_page): ?>
<?php			if(!array_key_exists('requires', $static_page)): ?>
						<li><a href="<?=$static_page['url'] ?>"><?php if(array_key_exists('icon', $static_page)): ?><i class="fa fw <?=$static_page['icon'] ?>" aria-hidden="true"></i> <?php endif; ?><?=$static_page['title']?></a></li>
<?php			endif; ?>
<?php		endforeach; ?>
					</ul>
<?php	endif; ?>
<?php	if(\Users\User::is_user_authenticated() && isset(Theme::theme()->include_in_navigation) && is_array(Theme::theme()->include_in_navigation)): ?>
					<ul>
<?php		foreach(Theme::theme()->include_in_navigation as $static_page): ?>
<?php			if(array_key_exists('requires', $static_page) && 'authorization' == $static_page['requires']): ?>
						<li><a href="<?=$static_page['url'] ?>"><?php if(array_key_exists('icon', $static_page)): ?><i class="fa fw <?=$static_page['icon'] ?>" aria-hidden="true"></i> <?php endif; ?><?=$static_page['title']?></a></li>
<?php			endif; ?>
<?php		endforeach; ?>
					</ul>
<?php	endif; ?>
<?php	if(count(Application::application()->routes) > 0): ?>
					<ul>
<?php
			foreach(Application::application()->routes as $key => $value):
				$class = $value . '\ViewController';
				if($class::_visible()): ?>
						<li><a href="/<?=$key?>"><?=$class::_navigation_slug()?></a></li>
<?php			endif;
			endforeach; ?>
					</ul>
<?php	endif;?>
				</nav>
			</aside>
		</div>
<?php	if(NULL != Theme::theme()->copyright): ?>
		<footer>
			<p><?=Theme::theme()->copyright ?></p>
		</footer>
<?php	endif;?>
<?php
	$scripts = Theme::theme()->scripts;
	foreach($scripts as $script) {
		echo "\t\t<script src=\"" . $script . "\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";
	}
?>
	</body>
</html>